<?php

namespace App\Helpers;

class ApiResponse{

	/**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public static function returnJson($nameArr, $data, $status, $code){
        return response()->json([
            'status' => $status,
            $nameArr => $data          
        ], $code);
    }
}