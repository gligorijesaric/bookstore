<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserAuthController extends Controller
{
    /**
     * Logs in a user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        // introduce login data
        $data = $request->validate(
            [
                'email' => 'email|required',
                'password' => 'required'
            ]
        );     

        // try to log in
        if (!auth()->attempt($data)) {
            return response(
                [
                    'status' => 'ERROR',
                    'message' => 'Incorrect Details. Please try again',
                    'data' => null
                ], 401
            );                 
        }

        // introduce the access token
        $token = auth()->user()->createToken('API Token')->accessToken;  
        return response(
            [
                'status' => 'SUCCESS',
                'data' => [
                            'user' => auth()->user(),
                            'token' => $token
                        ]
            ], 200
        );
    }
}
