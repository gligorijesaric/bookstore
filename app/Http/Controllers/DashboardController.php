<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;
use App\Models\Book;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginView()
    {   
        return view('login');
    }

    /**
     * Handle account login request
     * 
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {        
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
   
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) { 
            // introduce the access token
            $token = auth()->user()->createToken('API Token')->accessToken;                          
            return response(
                [
                'status' => 'SUCCESS',
                'data' => [
                            'user' => auth()->user(),
                            'token' => $token
                        ]
                ], 200
            );
        }else{
            return response(
                [
                    'status' => 'ERROR',
                    'message' => 'Incorrect Details. Please try again',
                    'data' => null
                ], 401
            );        
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = Author::with('books')->get();
        $totalBooks = [];
        foreach ($authors as $key => $author) {
            $totalBooks[$author->id] = count($author->books);
        }
        return view('authors', ['totalBooks' => $totalBooks, 'user' => auth()->user()]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addAuthor()
    {   
        return view('add_author', ['user' => auth()->user()]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Author $id
     * @return \Illuminate\Http\Response
     */
    public function author(Request $request, $id)
    {
        $author = Author::where('id', $id)->first();
        if($author){
             return view('author', ["author_id" => $id, 'author_name' => $author->name, 'user' => auth()->user()]);
         }else{
             return view('404');
         }       
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Author $id
     * @return \Illuminate\Http\Response
     */
    public function editAuthor(Request $request, $id)
    {
        $author = Author::where('id', $id)->first();
        if($author){
             return view('edit_author', ['author' => $author, 'user' => auth()->user()]);
         }else{
             return view('404');
         }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book $id
     * @return \Illuminate\Http\Response
     */
    public function editBook(Request $request, $id)
    {
        $book = Book::where('id', $id)->first();
        $authors = Author::all();
        if($book){
             return view('edit_book', ['book' => $book, 'authors' => $authors, 'user' => auth()->user()]);
         }else{
             return view('404');
         }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book $id
     * @return \Illuminate\Http\Response
     */
    public function Book(Request $request, $id)
    {
        $book = Book::where('id', $id)->first();
        $authors = Author::all();
        if($book){
            return view('book', ['book' => $book, 'authors' => $authors, 'user' => auth()->user()]);
        }else{
            return view('404');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addBook()
    {
        $authors = Author::all();   
        return view('add_book', ['authors' => $authors, 'user' => auth()->user()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show(Dashboard $dashboard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function edit(Dashboard $dashboard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dashboard $dashboard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dashboard $dashboard)
    {
        //
    }
}
