<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Illuminate\Validation\Rule;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        return ApiResponse::returnJson('data', $books, 'SUCCESS', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
                        [   
                            'name' => 'required|unique:books,name,'.$request->input('name'),
                            'description' => 'required',
                            'publication_year' => 'required',
                            'author_id' => 'required|exists:authors,id,id,'.$request->input("author_id")
                        ]
        );

        $book = Book::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'publication_year' => $request->input('publication_year'),
            'author_id' => $request->input('author_id')
        ]);

        return ApiResponse::returnJson('data', $book, 'SUCCESS', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return ApiResponse::returnJson('data', $book, 'SUCCESS', 200); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $request->validate(
                        [ 
                            'name' => Rule::unique('books')->ignore($book->id),
                            'description' => 'required',
                            'publication_year' => 'required',
                            'author_id' => 'required|exists:authors,id,id,'.$request->input("author_id")
                        ]
        );

        $book->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'publication_year' => $request->input('publication_year'),
            'author_id' => $request->input('author_id')
        ]);

        return ApiResponse::returnJson('data', $book, 'SUCCESS', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return ApiResponse::returnJson('data', null, 'SUCCESS', 204);
    }
}
