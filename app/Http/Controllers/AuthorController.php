<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Illuminate\Validation\Rule;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = Author::all();
        return ApiResponse::returnJson('data', $authors, 'SUCCESS', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      

        $request->validate(
                        [   
                            'name' => 'required|unique:authors,name,'.$request->input('name')
                        ]
        );

        $author = Author::create([
            'name' => $request->input('name')
        ]);

        return ApiResponse::returnJson('data', $author, 'SUCCESS', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {        
        $author_book = Author::where('id', $author->id)->with('books')->get();
        return ApiResponse::returnJson('data', $author_book, 'SUCCESS', 200);       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author)
    {
        $request->validate(
                        [ 
                            'name' => Rule::unique('authors')->ignore($author->id)                 
                        ]
        );

        $author->update([
            'name' => $request->input('name')
        ]);
        return ApiResponse::returnJson('data', $author, 'SUCCESS', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author)
    {
        $author->delete();
        return ApiResponse::returnJson('data', null, 'SUCCESS', 204);
    }
}
