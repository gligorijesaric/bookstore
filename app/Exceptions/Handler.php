<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $exception, $request) { 
            if($exception->getMessage() == 'Route [login] not defined.'){
                return redirect('login');
            }

            return response()->json(['status' => 'ERROR',
                                    'message' => $this->getFiltered($exception->getMessage()),
                                    'data' => null
                                    ], $this->getCode($exception)
                                    );
            }); 
    }

    /**
     * Status code the exception handling callbacks for the application.
     *
     * @return int
     */
    private function getCode($exception)
    {
        $code = 0;
        foreach ((array)$exception as $key => $value) {                   
            if($key === 'status'){
                $code = $value;
            }elseif(is_callable(array($exception, 'getStatusCode'))){ 
                $code = $exception->getStatusCode();     
            }elseif(is_callable(array($exception, 'getCode')) && $exception->getCode() != 0){ 
                $code = $exception->getCode();      
            }
        }
        return ($code == 0) ? 501 : $code;
    }

    /**
     * Handle "No Query Results For Model", callbacks for the application.
     *
     * @return string
     */
    private function getFiltered($message)
    {
        switch ($message) {
            case str_contains($message, 'No query results for model [App\\Models\\Author]'):
                return str_replace("No query results for model [App\\Models\\Author]", "The requested author does not exist with id:", $message);
                break;
            case str_contains($message, 'No query results for model [App\\Models\\Book]'):
                return str_replace("No query results for model [App\\Models\\Book]", "The requested book does not exist with id:", $message);
                break;
            
            default:
               return $message;
                break;
        }
    }
}
