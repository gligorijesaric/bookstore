# Bookstore
Laravel API/APP with with authorization ( Passport )

### CRUD REST API with Laravel 
	* This example shows how one should restful service work
	* Through the developing process it was use Postman for testing endpoints 
	* Api support method: GET (data display), POST (data entry), PUT (updating data), DELETE (deleting data)

### Following are the Models

    * Author
    * Book

### Usage

	Setup the repository 

* git clone https://gitlab.com/gligorijesaric/bookstore.git
* cd bookstore
* composer install
* cp .env.example .env (if you clone on windows use: copy .env.example .env)
* php artisan key:generate
* php artisan cache:clear && php artisan config:clear 

### Database Setup
	We are going to pull in data from the database so make sure that you have your database setup

* mysql;
* create database bookstore;
* exit;

### .env
	Setup your database credentials in the .env file

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=bookstore
DB_USERNAME={USERNAME}
DB_PASSWORD={PASSWORD}
```


### Laravel Passport
Laravel Passport is an Oauth 2.0 server implementation for API authentication using Laravel.<br> 
Laravel Passport is used to secure API through different ways of authentication.<br> 
Laravel Passport interacts with the database because the clients and access tokens need to be stored somewhere.<br> 

* In app\Providers\AuthServiceProvider.php the token is set to last 10 minutes ( change if you want more or less ) using carbon.

### Custom JSON response
* in app\Exceptions\Handler.php it is set to catch exception end return customized response
* in app\Helpers\ApiResponse.php used for customized json response

### Front
The front is done based on some simple boostrap examples with javascript integration.<br> 
Tokens are stored in local storage, when they expire using the login modal, they are renewed

### Run Migration with Seeder

Added dummy data for seeders (authors, books) with relations and one user for administration

* then run the following command to create migrations in the database.

php artisan migrate:fresh --seed


### Run Passport Encryption keys

* In order to run Laravel Passport, we need ot make sure that we run the following command to generate client information for us.

php artisan passport:install

### Run project via composer

* php artisan serve

### Login 
To use the application, log in using:

* Username: gligorijesaric@gmail.com
* Password: developer2022

Or change the access data before starting the seeder ( database\seeders\UsersTableSeeder.php )

### API EndPoints
If you want to test api endpoints please use Postman collection https://www.getpostman.com/collections/36d6a5fb681ffd2b11e7 ( also replace localhost with your domain or ip address ): 

### Other information
	* This is a simple example of creating Api from scratch using Laravel framework
	* OAuth2 server implementation for this Laravel api example ( Laravel Passport ) 