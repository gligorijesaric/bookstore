<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Hash;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
    		'first_name' => 'Gligorije',
    		'last_name' => 'Saric',
    		'email' => 'gligorijesaric@gmail.com',
        	'password' =>  Hash::make('developer2022')
    	];
         \App\Models\User::create($data);
    }
}
