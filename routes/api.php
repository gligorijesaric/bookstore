<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\Auth\UserAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
   	[
        'prefix' => '/v1'
    ],
    function () {

	// endpoint for logging in a api user and get access token
	Route::post('/user/login',  [UserAuthController::class,'login']);

    Route::apiResource('/authors', AuthorController::class)->middleware(['auth:api']);
    Route::apiResource('/books', BookController::class)->middleware(['auth:api']);

    Route::fallback(function(){
	    return response()->json([
	        'message' => 'Page Not Found. If error persists, contact info@website.com'], 404);
	});

});