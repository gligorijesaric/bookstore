<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['web'])->group(function () {
	Route::get('/login', [DashboardController::class,'loginView']);
	Route::post('/login', [DashboardController::class,'login']);
    Route::get('/', [DashboardController::class,'index'])->middleware(['auth:web']);
    Route::get('author/{id}', [DashboardController::class,'author'])->middleware(['auth:web']);
    Route::get('edit-author/{id}', [DashboardController::class,'editAuthor'])->middleware(['auth:web']);
    Route::get('add-author', [DashboardController::class,'addAuthor'])->middleware(['auth:web']);
    Route::get('edit-book/{id}', [DashboardController::class,'editBook'])->middleware(['auth:web']);
    Route::get('book/{id}', [DashboardController::class,'Book'])->middleware(['auth:web']);
    Route::get('add-book', [DashboardController::class,'addBook'])->middleware(['auth:web']);
    Route::get('/logout', function(){
	   Auth::logout();
	   return Redirect::to('login');
	});

	Route::fallback(function(){
	     return view('404');
	});
});
