<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bookstore::Single Author ( {{ $author_name }} )</title>  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/fonts/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/fonts/material-design-iconic-font.min.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/css/util.css">
  <link rel="stylesheet" type="text/css" href="{{url('/')}}/css/main.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="//cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
  <link rel="stylesheet" href="//cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
  <link rel="shortcut icon" type="image/png" href="{{url('/')}}/img/book.png">
  <style type="text/css">
  /* If you like this, be sure to ❤️ it. */
.modal {
  visibility: hidden;
  opacity: 0;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background: rgba(77, 77, 77, .7);
  transition: all .4s;
}

.modal:target {
  visibility: visible;
  opacity: 1;
}

.modal__content {
  border-radius: 4px;
  position: relative;
  width: 500px;
  max-width: 90%;
  background: #fff;
  padding: 1em 2em;
}

.modal__footer {
  text-align: right;
  a {
    color: #585858;
  }
  i {
    color: #d02d2c;
  }
}
.modal__close {
  position: absolute;
  top: 10px;
  right: 10px;
  color: #585858;
  text-decoration: none;
}

.p-t-85 {
  padding-top: 0px;
}

.input100 {  
  border:none;
  border-bottom: 1px solid #57b846; 
}

</style>
</head>
<body>

<div class="container">
  <div style="display:flex; justify-content:space-between;">
      <div>
        <h2><span class="glyphicon glyphicon-user"></span> {{ $author_name }}</h2>
        <p>List of all authors books with pagination and actions (add, edit, delete, search)</p>   
      </div>
      <div style="margin-top: 2%;">
        <a href="{{url('/')}}/logout"><button type="button" class="btn btn-danger btn-sm px-3" style="">
          <span class="glyphicon glyphicon-log-out"></span> Logout
        </button></a>
        <p>{{$user->first_name}} {{$user->last_name}}</p>
      </div>
  </div>
  
  <table class="table" id="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Book Name</th>
        <th>Description</th>
        <th>Year of publication</th>
        <th>Created</th>       
        <th style="text-align: center;">Actions</th>
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
</div>

<div id="validate-modal" class="modal">
    <div class="modal__content">
        <h1 id="api_resp"></h1>
        <div id="status_code">
            
        </div>   
        <a href="#" class="modal__close">&times;</a>
    </div>
</div>

<!-- The Modal -->
<div id="tokenModal" class="modal"> 
  <!-- Modal content -->
  <div class="modal__content">   
    <a href="#" onclick="Logout()" class="modal__close">&times;</a>
    <p>Your token has expired, do you want to refresh it to continue with the action ?!</p>
      <label for="email">Username</label>
      <input class="input100 has-val" type="email" name="email" id="email">

      <label style="margin-top: 5px;" for="password">Password</label>
      <input class="input100 has-val" type="password" name="pass" id="password"> 
      <input type="hidden" name="book_id" id="book_id">          
      <br>
      <button class="login100-form-btn" onclick="ReloadToken()">Login</button> 
      <p style="color: red; text-align: center; margin-top: 5px; margin-bottom: 5px;" id="auth_resp"></p>   
  </div>
</div>


<script>
  FillTable();
  function format(inputDate) 
  {
    let date, month, year;

    date = inputDate.getDate();
    month = inputDate.getMonth() + 1;
    year = inputDate.getFullYear();

      date = date
          .toString()
          .padStart(2, '0');

      month = month
          .toString()
          .padStart(2, '0');

    return `${year}/${month}/${date}`;
  }

  function removeBook(book_id)
  {
    var data = null;
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.addEventListener("readystatechange", function () {
   
      //Displaying an error in modal if it fails to delete 
      try {
        var apiResponse = JSON.parse(xhr.response);
        if( apiResponse.status == 'ERROR'){
          window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'')+"#tokenModal";
          document.getElementById('book_id').value = book_id; 
          return;           
        }   
      } catch (e) { }  
      
      //Displaying new data information after delete 
      if( this.readyState == 4 && this.status == 204){  
        $('#table').DataTable().destroy();
          FillTable();                      
          return;  
      }
        
    });

    xhr.open("DELETE", "{{url('/')}}/api/v1/books/"+book_id);
    xhr.setRequestHeader("Authorization", "Bearer "+localStorage.getItem("bookstore-token"));
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(data);           
  }

  function FillTable()
  { 
	var xhr = new XMLHttpRequest();
	xhr.withCredentials = true;
	xhr.addEventListener("readystatechange", function () {
	if (this.readyState === 4) {
    
    try {  
      var apiResponse = JSON.parse(xhr.response);
      //Displaying an error in modal if it fails to get information from api          
      if( apiResponse.status === 'ERROR'){
        window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'')+"#validate-modal"; 
        localStorage.removeItem("bookstore-token");                         
        document.getElementById("status_code").innerHTML = apiResponse.status+' ('+xhr.status+')';
        document.getElementById("api_resp").innerHTML = apiResponse.message;
        $("#validate-modal").addClass("modal in");
        $("#validate-modal").css("display","");                    
        return;  
      }   
      //Displaying information in DataTable
      if( apiResponse.status === 'SUCCESS'){ 
        $('#table').DataTable( {
            'fnDrawCallback': function () {    
                if($('#addBTN').length ===0){     
                  $('.dataTables_filter').append(' <a href="/"><button type="button" class="btn btn-primary" id="addBTN" title="Back to Authors"><span class="glyphicon glyphicon-user"></span> Authors</button></a>'
                      +' <a href="/add-book"><button type="button" class="btn btn-success" id="addBTN" title="Add New"><span class="glyphicon glyphicon-plus"></span> Add Book</button></a>'
                    );
                }
            }, 
            "data": apiResponse.data[0].books,                
                "columns": [
                      { "data":"id"},
                      { "data":"name",},
                      { "data":"description"},
                      { "data":"publication_year"},
                      { data: 'created_at',
                        render: function (data, type, row) {          
                            return format(new Date(data));                  
                        }
                    },
                    { "data": null,            
                        "render": function (data, type, row) {
                              return '<a style="text-decoration: none;" href="{{url("/")}}/book/'+row.id+'" title="View Book"><button type="button" class="btn btn-outline-primary"><span class="glyphicon glyphicon-book"></span> View</a> | '
                                  +'<a href="{{url("/")}}/edit-book/'+row.id+'" title="Edit Author"><span class="glyphicon glyphicon-edit"></span> Edit | </a>'
                                + '<a style="text-decoration: none; color:red;" onclick="removeBook('+row.id+')"; href="#" title="Delete Book"><span class="glyphicon glyphicon-remove-circle"></span> Delete</a>'; 
                        }
                    },           
                
                ],
        } );        
      }   
    } catch (e) {;
      //Call login modal if token is expired ( get redirect from api to login )
      localStorage.removeItem("bookstore-token");                         
      window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'')+"#tokenModal"; 
      $("#tokenModal").addClass("modal in");
      $("#tokenModal").css("display","");                      
      return; 
    }
	}
	});

	xhr.open("GET", "{{url('/')}}/api/v1/authors/"+<?= $author_id; ?>);
  xhr.setRequestHeader("Authorization", "Bearer "+localStorage.getItem("bookstore-token"));
	xhr.setRequestHeader("cache-control", "no-cache");
	xhr.send();
       
  }

  function ReloadToken()
  {
    $("#tokenModal").removeClass("in");
    $("#tokenModal").css("display","none");
    var username = document.getElementById('email').value;
    var password = document.getElementById('password').value; 
    var xhr = new XMLHttpRequest();
        var params = 'email='+username+'&password='+password+'&_token='+'<?= csrf_token(); ?>';
        xhr.open('POST', "{{url('/')}}/api/v1/user/login", true);

        //Send the proper header information along with the request
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        xhr.onload = function() {//Call a function when the state changes.
          if (this.readyState === 4) {
              var apiResponse = JSON.parse(xhr.response);
              //Save new toke on logal storage and repeat action ( the login was successful )           
              if(apiResponse.status === 'SUCCESS'){                               
                localStorage.setItem("bookstore-token", apiResponse.data.token); 
                window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'');  
                if(document.getElementById('book_id').value != ''){
                  var book_id = document.getElementById('book_id').value;
                  removeBook(book_id);
                  document.getElementById('book_id').value = '';
                }else{
                  FillTable();   
                }             
              } 
              //Displaying an error on modal if the login was not successful
              if( apiResponse.status === 'ERROR'){
                document.getElementById('auth_resp').innerHTML = apiResponse.message+' ('+xhr.status+')';
                $("#tokenModal").addClass("modal in");
                $("#tokenModal").css("display","");                    
                return;  
              } 
          }
        }
        xhr.send(params);  
  }

  function Logout()
  {
    localStorage.removeItem("bookstore-token");
    window.location.replace('/login');
  }
</script>
</body>
</html>