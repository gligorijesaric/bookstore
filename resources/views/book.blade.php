<!DOCTYPE html>
<html lang="en">
<head>
<title>Bookstore::Single Book ( {{ $book->name }} )</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="{{url('/')}}/img/book.png">
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/util.css">
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/main.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<meta name="robots" content="noindex, follow">
<style type="text/css">
.p-t-85 {
  padding-top: 0px;
}
</style>
</head>
<body>

<div class="container">
  <div style="display:flex; justify-content:space-between;">
      <div>
        <h2><span class="glyphicon glyphicon-book"></span> View Book ( {{ $book->name }} )</h2>
        <p>Detailed information about the book itself</p>   
      </div>
      <div style="margin-top: 2%;">      						
		<a style="text-decoration: none;" href="{{url('/')}}/author/{{$book->author_id}}" class="txt2"><span class="txt1">Author</span> <span class="glyphicon glyphicon-arrow-left"></span></a>&nbsp;&nbsp;&nbsp;		
        <a href="{{url('/')}}/logout"><button type="button" class="btn btn-danger btn-sm px-3" style="">
          <span class="glyphicon glyphicon-log-out"></span> Logout
        </button></a>
        <p>{{$user->first_name}} {{$user->last_name}}</p>
      </div>
  </div>
</div>

<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100 p-t-85 p-b-20">
				<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate="Enter name">
					<input class="input100 has-val" type="text" name="name" id="name" value="{{$book->name}}" readonly>
					<span class="focus-input100" data-placeholder="Name"></span>
				</div>	

				<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate="Enter description ">
					<textarea class="input100 has-val" type="text" name="description " id="description" readonly>{{$book->description}}</textarea>
					<span class="focus-input100" data-placeholder="Description"></span>
				</div>

				<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate="Enter Year of publication">
					<input class="input100 has-val" type="text" name="publication_year" id="publication_year" value="{{$book->publication_year}}" readonly>
					<span class="focus-input100" data-placeholder="Year of publication"></span>
				</div>		

				<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate="Author">
						@foreach($authors as $author)
						    @if($author->id === $book->author_id)  							
								<input class="input100 has-val" type="text" name="author" id="author" value="{{$author->name}}" readonly>
							@endif
						@endforeach	
					<span class="focus-input100" data-placeholder="Author"></span>
				</div>		
		</div>
	</div>
</div>

</body>
</html>
