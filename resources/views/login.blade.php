<!DOCTYPE html>
<html lang="en">
<head>
<title>Bookstore::Login</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="{{url('/')}}/img/book.png">
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/util.css">
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/main.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<meta name="robots" content="noindex, follow">
<style type="text/css">
	/* If you like this, be sure to ❤️ it. */
.modal {
  visibility: hidden;
  opacity: 0;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background: rgba(77, 77, 77, .7);
  transition: all .4s;
}

.modal:target {
  visibility: visible;
  opacity: 1;
}

.modal__content {
  border-radius: 4px;
  position: relative;
  width: 500px;
  max-width: 90%;
  background: #fff;
  padding: 1em 2em;
}

.modal__footer {
  text-align: right;
  a {
    color: #585858;
  }
  i {
    color: #d02d2c;
  }
}
.modal__close {
  position: absolute;
  top: 10px;
  right: 10px;
  color: #585858;
  text-decoration: none;
}

.p-t-85 {
  padding-top: 0px;
}
</style>
</head>
<body>

<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100 p-t-85 p-b-20">	
				<span class="login100-form-title p-b-70">Please Login</span>
					<span class="login100-form-avatar">
						<img src="{{url('/')}}/img/book.png" alt="AVATAR">
					</span>

				<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate="Enter username">
					<input class="input100 has-val" type="email" name="email" id="email">
					<span class="focus-input100" data-placeholder="Username"></span>
				</div>

				<div class="wrap-input100 validate-input m-b-50" data-validate="Enter password">
					<input class="input100" type="password" name="pass" id="password">
					<span class="focus-input100" data-placeholder="Password"></span>
				</div>

				<div class="container-login100-form-btn">
					<button class="login100-form-btn" onclick="Login(); return false;">Login</button>
				</div>

				<ul class="login-more p-t-190">
					<li class="m-b-8">
						<span class="txt1">Forgot</span>
					<a href="/forgot" class="txt2">Username / Password?</a>
				</li>				
				</ul>
		</div>
	</div>
</div>

<div id="validate-modal" class="modal">
    <div class="modal__content">
        <h1 id="api_resp"></h1>
        <div id="status_code">
            
        </div>   
        <a href="#" class="modal__close">&times;</a>
    </div>
</div>

<script src="{{url('/')}}/js/jquery-3.2.1.min.js"></script>
<script src="{{url('/')}}/js/main.js"></script>

<script type="text/javascript">
	function Login()
	{
		var username = document.getElementById('email').value;
		var password = document.getElementById('password').value;
		if(username === '' || password === ''){
			let inps = 'One or more fields are required:'
			           +'<p style="color:red">* Username is required</p>'
			           +'<p style="color:red">* Password is required</p>'
			           +'<br>Please add required fields !';
			document.getElementById("status_code").innerHTML = 'Error ( 422 )';
			document.getElementById("api_resp").innerHTML = inps;
			// $('#validate-modal').modal('show'); // for some reason it's not working right now (it's probably a problem with bootstrap.min.js )
			window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'')+"#validate-modal";
		}else{
				var xhr = new XMLHttpRequest();
				var params = 'email='+username+'&password='+password+'&_token='+'<?= csrf_token(); ?>';
				xhr.open('POST', "{{url('/')}}/login", true);

				//Send the proper header information along with the request
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

				xhr.onload = function() {//Call a function when the state changes.
					if (this.readyState === 4) {
					    var apiResponse = JSON.parse(xhr.response);						
					    document.getElementById("status_code").innerHTML = apiResponse.status+' ('+xhr.status+')';
					    if(apiResponse.status !== 'SUCCESS'){
					    	document.getElementById("api_resp").innerHTML = apiResponse.message;
					    	// $('#validate-modal').modal('show'); // for some reason it's not working right now (it's probably a problem with bootstrap.min.js )
					    window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'')+"#validate-modal";		
					    }else{
					    	localStorage.setItem("bookstore-token", apiResponse.data.token);
					    	window.location.replace('/');					    	
					    }	    					    
				    }
				}
				xhr.send(params); 
		}
	}

</script>

</body>
</html>
