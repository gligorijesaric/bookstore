<!DOCTYPE html>
<html lang="en">
<head>
<title>Bookstore::Add Book</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="{{url('/')}}/img/book.png">
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/util.css">
<link rel="stylesheet" type="text/css" href="{{url('/')}}/css/main.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<meta name="robots" content="noindex, follow">
<style type="text/css">
	/* If you like this, be sure to ❤️ it. */
.modal {
  visibility: hidden;
  opacity: 0;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background: rgba(77, 77, 77, .7);
  transition: all .4s;
}

.modal:target {
  visibility: visible;
  opacity: 1;
}

.modal__content {
  border-radius: 4px;
  position: relative;
  width: 500px;
  max-width: 90%;
  background: #fff;
  padding: 1em 2em;
}

.modal__footer {
  text-align: right;
  a {
    color: #585858;
  }
  i {
    color: #d02d2c;
  }
}
.modal__close {
  position: absolute;
  top: 10px;
  right: 10px;
  color: #585858;
  text-decoration: none;
}

.p-t-85 {
  padding-top: 0px;
}

.input100 {  
  border:none;
  border-bottom: 1px solid #57b846; 
}

</style>
</head>
<body>

<div class="container">
  <div style="display:flex; justify-content:space-between;">
      <div>
        <h2><span class="glyphicon glyphicon-book"></span> Add Book</h2>
        <p>Fill the necessary information and then go to insert button</p>   
      </div>
      <div style="margin-top: 2%;">      						
		<a style="text-decoration: none;" href="{{url('/')}}" class="txt2"><span class="txt1">Authors</span> <span class="glyphicon glyphicon-arrow-left"></span></a>&nbsp;&nbsp;&nbsp;		
        <a href="{{url('/')}}/logout"><button type="button" class="btn btn-danger btn-sm px-3" style="">
          <span class="glyphicon glyphicon-log-out"></span> Logout
        </button></a>
        <p>{{$user->first_name}} {{$user->last_name}}</p>
      </div>
  </div>
</div>

<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100 p-t-85 p-b-20">
			<form class="login100-form validate-form">	
				<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate="Enter name">
					<input class="input100 has-val" type="text" name="name" id="name">
					<span class="focus-input100" data-placeholder="Name"></span>
				</div>	

				<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate="Enter description ">
					<textarea class="input100 has-val" type="text" name="description " id="description"></textarea>
					<span class="focus-input100" data-placeholder="Description"></span>
				</div>

				<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate="Enter Year of publication">
					<input class="input100 has-val" type="text" name="publication_year" id="publication_year">
					<span class="focus-input100" data-placeholder="Year of publication"></span>
				</div>		

				<div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate="Select Author">
					<select class="input100 has-val" name="author" id="author">
						<option value="">Please select</option>
						@foreach($authors as $author)
							<option value="{{$author->id}}">{{$author->name}}</option>
						@endforeach					  
					</select>
					<span class="focus-input100" data-placeholder="Authors"></span>
				</div>		

				<div class="container-login100-form-btn">
					<button class="login100-form-btn" onclick="addBook(); return false;">Insert</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="validate-modal" class="modal">
    <div class="modal__content">
        <h1 id="api_resp"></h1>
        <div id="status_code">
            
        </div>   
        <a href="#" class="modal__close">&times;</a>
    </div>
</div>

<!-- The Modal -->
<div id="tokenModal" class="modal"> 
  <!-- Modal content -->
  <div class="modal__content">   
    <a href="#" onclick="Logout()" class="modal__close">&times;</a>
    <p>Your token has expired, do you want to refresh it to continue with the action ?!</p>
      <label for="email">Username</label>
      <input class="input100 has-val" type="email" name="email" id="email">

      <label style="margin-top: 5px;" for="password">Password</label>
      <input class="input100 has-val" type="password" name="pass" id="password">           
      <br>
      <button class="login100-form-btn" onclick="ReloadToken()">Login</button> 
      <p style="color: red; text-align: center; margin-top: 5px; margin-bottom: 5px;" id="auth_resp"></p>   
  </div>
</div>


<script type="text/javascript">
	function addBook()
	{
		var name = document.getElementById('name').value;		
		if(name === ''){
			let inps = 'One or more fields are required:'
			           +'<p style="color:red">* Name is required</p>'
			           +'<p style="color:red">* Description is required</p>'
			           +'<p style="color:red">* Year of publication is required</p>'
			           +'<p style="color:red">* Author is required</p>'
			           +'Please add required fields !';
			document.getElementById("status_code").innerHTML = 'Error ( 422 )';
			document.getElementById("api_resp").innerHTML = inps;
			// $('#validate-modal').modal('show'); // for some reason it's not working right now (it's probably a problem with bootstrap.min.js )
			window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'')+"#validate-modal";
		}else{
				var xhr = new XMLHttpRequest();
				var params = 'name='+name+'&description='+description.value+'&publication_year='+publication_year.value+'&author_id='+author.value;
				xhr.open('POST', "{{url('/')}}/api/v1/books/", true);

				//Send the proper header information along with the request
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        		xhr.setRequestHeader("Authorization", "Bearer "+localStorage.getItem("bookstore-token"));

				xhr.onload = function() {//Call a function when the state changes.
					if (this.readyState === 4) {					    
						    try {  
						    var apiResponse = JSON.parse(xhr.response);
						    //Displaying an error in modal if it fails to save the new entry					   
			                if( apiResponse.status === 'ERROR'){
			                	window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'')+"#validate-modal"; 
			                    localStorage.removeItem("bookstore-token");			                    
			                    document.getElementById("status_code").innerHTML = apiResponse.status+' ('+xhr.status+')';
			                    document.getElementById("api_resp").innerHTML = apiResponse.message;
                				$("#validate-modal").addClass("modal in");
                				$("#validate-modal").css("display","");                    
			                    return;  
			                }   
			                //Displaying information that a new entry has been successfully saved
			                if( apiResponse.status === 'SUCCESS'){	
			                	window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'')+"#validate-modal"; 
			                    document.getElementById("status_code").innerHTML = apiResponse.status+' ('+xhr.status+')';
			                    document.getElementById("api_resp").innerHTML = 'You have successfully added a book';
                				$("#validate-modal").addClass("modal in");
                				$("#validate-modal").css("display","");                    
			                  return;  
			                }   
			            } catch (e) {
			            	  //Call login modal if token is expired ( get redirect from api to login )
			            	  localStorage.removeItem("bookstore-token");			                    
			                  window.location = (""+window.location).replace(/#[A-Za-z0-9_]*$/,'')+"#tokenModal"; 
			                  $("#tokenModal").addClass("modal in");
                			  $("#tokenModal").css("display","");                      
			                  return; 
			            }
					}
				}
			xhr.send(params); 
		}
	}

  function ReloadToken()
  {
    $("#tokenModal").removeClass("in");
    $("#tokenModal").css("display","none");
    var username = document.getElementById('email').value;
    var password = document.getElementById('password').value; 
    var xhr = new XMLHttpRequest();
        var params = 'email='+username+'&password='+password+'&_token='+'<?= csrf_token(); ?>';
        xhr.open('POST', "{{url('/')}}/api/v1/user/login", true);

        //Send the proper header information along with the request
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        xhr.onload = function() {//Call a function when the state changes.
          if (this.readyState === 4) {
              var apiResponse = JSON.parse(xhr.response);
              //Save new toke on logal storage and repeat action to add entry ( the login was successful )           
              if(apiResponse.status === 'SUCCESS'){               
                localStorage.setItem("bookstore-token", apiResponse.data.token); 
                addBook();                
              } 
              //Displaying an error on modal if the login was not successful
              if( apiResponse.status === 'ERROR'){
                document.getElementById('auth_resp').innerHTML = apiResponse.message+' ('+xhr.status+')';
                $("#tokenModal").addClass("modal in");
                $("#tokenModal").css("display","");                    
                return;  
              } 
          }
        }
        xhr.send(params);  
  }

  function Logout()
  {
    localStorage.removeItem("bookstore-token");
    window.location.replace('/login');
  }
</script>

<script src="{{url('/')}}/js/main.js"></script>
<script src="{{url('/')}}/js/popper.js"></script>

</body>
</html>
